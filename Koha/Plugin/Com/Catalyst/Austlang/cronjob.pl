#!/usr/bin/perl

use Koha::Plugins;
use Koha::Plugin::Com::Catalyst::Austlang;

my $plugin = Koha::Plugin::Com::Catalyst::Austlang->new();
$plugin->download_import_austlang();
