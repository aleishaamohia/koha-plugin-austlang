package Koha::Plugin::Com::Catalyst::Austlang;

## It's good practice to use Modern::Perl
use Modern::Perl;

## Required for all plugins
use base qw( Koha::Plugins::Base );

## We will also need to include any Koha libraries we want to access
use Modern::Perl;
use LWP::Simple qw( getstore );
use Text::CSV_XS;
 
use Koha::AuthorisedValueCategories;
use Koha::AuthorisedValues;
use C4::Context;
 
## Here we set our plugin version
our $VERSION = "1.0";

## Here is our metadata, some keys are required, some are optional
our $metadata = {
    name            => "Austlang",
    author          => "Aleisha Amohia",
    description     => "Adds a new cronjob that can be scheduled to regularly load Indigenous Australian languages (Austlang) data from an external source, process it, then create or update authorised values of this data. It adds an AUSTLANG authorised value category which can be used for the Austlang data.",
    date_authored   => "2024-02-26",
    date_updated    => "2024-02-26",
    minimum_version => "22.11",
    maximum_version => undef,
    version         => $VERSION,
};


## This is the minimum code required for a plugin's 'new' method
## More can be added, but none should be removed
sub new {
    my ( $class, $args ) = @_;

    ## We need to add our metadata here so our base class can access it
    $args->{'metadata'} = $metadata;

    ## Here, we call the 'new' method for our base class
    ## This runs some additional magic and checking
    ## and returns our actual $self
    my $self = $class->SUPER::new($args);

    return $self;
}

sub install {
    my ( $self, $args ) = @_;

    return C4::Context->dbh->do(
        q{ INSERT IGNORE INTO authorised_value_categories (category_name, is_system) VALUES ('AUSTLANG',0) }
    );
}

sub uninstall {
    my ( $self, $args ) = @_;

    C4::Context->dbh->do(
        q{ DELETE FROM authorised_values WHERE category = "AUSTLANG" }
    );

    return C4::Context->dbh->do(
        q{ DELETE FROM authorised_value_categories WHERE category_name = "AUSTLANG" }
    );
}

sub configure {
    my ( $self, $args ) = @_;

    my $cgi = $self->{'cgi'};
    my $template = $self->get_template({ file => 'configure.tt' });

    if ( $cgi->param('save') ) {
	    my $url      = $cgi->param('url') || "https://data.gov.au/data/dataset/70132e6f-259c-4e0f-9f95-4aed1101c053/resource/e9a9ea06-d821-4b53-a05f-877409a1a19c/download/aiatsis_austlang_endpoint_001.csv";
	    my $file     = $cgi->param('file') || '/tmp/austlang.csv';
	    my $category = $cgi->param('category') || 'AUSTLANG';
	    my $update   = $cgi->param('update') || undef;
	    my $verbose  = $cgi->param('verbose') || undef;

	    $self->store_data({ url => $url });
	    $self->store_data({ file => $file });
	    $self->store_data({ category => $category });
	    $self->store_data({ update => $update });
	    $self->store_data({ verbose => $verbose });

	    $self->go_home();
    } else {
	    my $url = $self->retrieve_data('url') || "https://data.gov.au/data/dataset/70132e6f-259c-4e0f-9f95-4aed1101c053/resource/e9a9ea06-d821-4b53-a05f-877409a1a19c/download/aiatsis_austlang_endpoint_001.csv";
	    my $file = $self->retrieve_data('file') || '/tmp/austlang.csv';
	    my $avc  = $self->retrieve_data('category') || 'AUSTLANG';
	    my $verbose = $self->retrieve_data('verbose') || undef;
	    my $update = $self->retrieve_data('update') || undef;

	    $template->param(
		cronjob => $self->mbf_path('cronjob.pl'),
		url => $url,
		file => $file,
		category => $avc,
		update => $update,
		verbose => $verbose,
	    );
    }

    print $cgi->header();
    print $template->output();
}

sub download_import_austlang {
    my ( $self, $args ) = @_;

    my $url = $self->retrieve_data('url') || "https://data.gov.au/data/dataset/70132e6f-259c-4e0f-9f95-4aed1101c053/resource/e9a9ea06-d821-4b53-a05f-877409a1a19c/download/aiatsis_austlang_endpoint_001.csv";
    my $file = $self->retrieve_data('file') || '/tmp/austlang.csv';
    my $avc  = $self->retrieve_data('category') || 'AUSTLANG';
    my $verbose = $self->retrieve_data('verbose') || undef;
    my $update = $self->retrieve_data('update') || undef;
    
    my $code = getstore( $url, $file );
     
    my $csv = Text::CSV_XS->new(
        {
            quote_char         => '"',
            auto_diag          => 1,
            allow_whitespace   => 1,
            binary             => 1,
            allow_loose_quotes => 1,
        }
    );
     
    my $austlang_avc = Koha::AuthorisedValueCategories->find( { category_name => $avc } );
    if ($austlang_avc) {
     
        if ( $code == 200 ) {
            my $av_count     = 0;
            my $update_count = 0;
     
            open my $fh, "<", $file;
     
            my $header = $csv->getline($fh);
     
            while ( my $row = $csv->getline($fh) ) {
     
                my $av       = $row->[0];
                my $lib      = $row->[1];
                my $lib_opac = $row->[1];
     
                my $av_already_exists =
                  Koha::AuthorisedValues->find( { authorised_value => $av } );
                if ($av_already_exists) {
                    if ($update) {
                        $av_already_exists->update(
                            {
                                lib      => $lib,
                                lib_opac => $lib_opac,
                            }
                        );
     
                        print "Existing $avc authorised value updated: $av - $lib\n"
                          if $verbose;
     
                        $update_count++;
                    }
                    else {
                        print "Authorised value match found, not updating: $av\n"
                          if $verbose;
                    }
                }
                else {
                    my $austlang_av = Koha::AuthorisedValue->new(
                        {
                            category         => $avc,
                            authorised_value => $av,
                            lib              => $lib,
                            lib_opac         => $lib_opac,
                        }
                    )->store;
     
                    print "New $avc authorised value added: $av - $lib\n"
                      if $verbose;
     
                    $av_count++;
                }
            }
            print
    "Job completed, with $av_count new authorised values loaded for $avc\n"
              if $verbose;
            print "Updated $update_count authorised values for $avc\n"
              if ( $verbose and $update );
        }
        else {
            print "Error accessing data.\n";
        }
    }
    else {
        print "No $avc authorised value category found.\n";
    }
     
    unlink $file;
}
