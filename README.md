# Austlang Koha plugin

## Introduction

This plugin adds a new cronjob that can be scheduled to regularly load Indigenous Australian languages (Austlang) data from an external source, process it, then create or update authorised values of this data. When installed, it adds an AUSTLANG authorised value category which can be used for the Austlang data.

## Downloading

Download the latest `*.kpz` file in the base directory of this project, or from the [Releases page](https://gitlab.com/aleishaamohia/koha-plugin-austlang/-/releases).

## Installing

The plugin system needs to be turned on by a system administrator.

To set up the Koha plugin system you must first make some changes to your installation.

* Change `<enable_plugins>0<enable_plugins>` to `<enable_plugins>1</enable_plugins>` in your `koha-conf.xml` file
* Confirm that the path to `<pluginsdir>` exists, is correct, and is writable by the web server
* Restart your webserver

Once these steps are complete, you can go to the staff interface in your browser, go to Koha Administration -> Manage plugins, and upload the `*.kpz` file.

The AUSTLANG authorised value category is created in Koha as part of the plugin installation.

## Configuring

There are a number of settings available for this plugin.

* Dataset source URL, default: "https://data.gov.au/data/dataset/70132e6f-259c-4e0f-9f95-4aed1101c053/resource/e9a9ea06-d821-4b53-a05f-877409a1a19c/download/aiatsis_austlang_endpoint_001.csv"
* Filename for downloaded file, default: "/tmp/austlang.csv"
* Authorised value category, default: AUSTLANG
* How to handle authorised value matches, default: "Skip matches from dataset"
* Print verbose job messages, default: disabled

## Run the script manually

Ask your system administrator to run the following commands:

`sudo koha-shell <instancename>`

`/var/lib/koha/<instancename>/plugins/Koha/Plugin/Com/Catalyst/Austlang/cronjob.pl`

## Schedule the script to run daily

Ask your system administrator to add the following line to the crontab, which configures this job to run daily:

`59 23 * * * root koha-foreach --chdir --enabled /var/lib/koha/<instancename>/plugins/Koha/Plugin/Com/Catalyst/Austlang/cronjob.pl`

If you opt to not add this line to your crontab, you will need to manually add Austlang values to the AUSTLANG authorised value category. 

## Uninstalling

This will delete all AUSTLANG authorised values as well as the AUSTLANG authorised value category. It will not however change any frameworks using the AUSTLANG authorised value.

## Packaging

To make the `.kpz` file:

`zip -r koha-plugin-austlang.kpz Koha/`

## Author

Aleisha Amohia <aleisha@catalyst.net.nz>

## Sponsorship

Education Services Australia SCIS
